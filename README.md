#Intelligent Sensing for Autonomous UAV

#SJSU Research Project

GitLab:

#Members
Professor: Dr. Kaikai Liu <kaikai.liu@sjsu.edu>

Niraj Surati <niraj.surati@sjsu.edu>
Ashwini Bhargava <ashwini.bhargava@sjsu.edu>
Himanshu Gunjal <himanshu.gunjal@sjsu.edu>
Neel Patel <neel.patel@sjsu.edu>

#Hardware
1. Tarot X8
2. J120 Carrier board 
3. Jetson Nvidia TX2 - Build and flash Jetpack 3.3 with kernel patch to enable USB3 ports

#Build Instructions



#Run Debug


#Sensors Used
1. ZED camera
2. TI MMWAVE AWR1642
3. Intel Realsense D435i

#Drone Build and Run
#On Jetson TX2
1. cd mavros_ws
2. source /opt/ros/kinetic/setup.bash
3. catkin build
4. run the ros nodes for the cameras and radar
    './devel/lib/'
    './devel/lib/'
    './devel/lib/'
    
#Navigation

#Sensor - RADAR
1. Integrate with ROS Kinetic
2. Select launch files for application - short range/long range etc.
3. Visualize results in Rviz
4. Extract required data by subscribing to rostopic radar_scan

#Integration for path planning and obstacle avoidance


