# CMake generated Testfile for 
# Source directory: /home/niraj/Radar_ws/src/serial/tests
# Build directory: /home/niraj/Radar_ws/build/serial/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_serial_gtest_serial-test "/home/niraj/Radar_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/niraj/Radar_ws/build/test_results/serial/gtest-serial-test.xml" "--return-code" "/home/niraj/Radar_ws/devel/lib/serial/serial-test --gtest_output=xml:/home/niraj/Radar_ws/build/test_results/serial/gtest-serial-test.xml")
add_test(_ctest_serial_gtest_serial-test-timer "/home/niraj/Radar_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/niraj/Radar_ws/build/test_results/serial/gtest-serial-test-timer.xml" "--return-code" "/home/niraj/Radar_ws/devel/lib/serial/serial-test-timer --gtest_output=xml:/home/niraj/Radar_ws/build/test_results/serial/gtest-serial-test-timer.xml")
