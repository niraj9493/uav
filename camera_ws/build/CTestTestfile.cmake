# CMake generated Testfile for 
# Source directory: /home/niraj/Radar_ws/src
# Build directory: /home/niraj/Radar_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(serial)
subdirs(ti_mmwave_rospkg)
subdirs(rviz)
