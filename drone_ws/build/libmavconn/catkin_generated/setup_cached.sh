#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/media/nvidia/D216D77016D753D7/drone_ws/devel/.private/libmavconn:$CMAKE_PREFIX_PATH"
export PWD="/media/nvidia/D216D77016D753D7/drone_ws/build/libmavconn"
export PYTHONPATH="/media/nvidia/D216D77016D753D7/drone_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/media/nvidia/D216D77016D753D7/drone_ws/devel/.private/libmavconn/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/media/nvidia/D216D77016D753D7/drone_ws/src/mavros/libmavconn:$ROS_PACKAGE_PATH"