# CMake generated Testfile for 
# Source directory: /media/nvidia/D216D77016D753D7/drone_ws/src/serial/tests
# Build directory: /media/nvidia/D216D77016D753D7/drone_ws/build/serial/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_serial_gtest_serial-test "/media/nvidia/D216D77016D753D7/drone_ws/build/serial/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/media/nvidia/D216D77016D753D7/drone_ws/build/serial/test_results/serial/gtest-serial-test.xml" "--return-code" "/media/nvidia/D216D77016D753D7/drone_ws/devel/.private/serial/lib/serial/serial-test --gtest_output=xml:/media/nvidia/D216D77016D753D7/drone_ws/build/serial/test_results/serial/gtest-serial-test.xml")
add_test(_ctest_serial_gtest_serial-test-timer "/media/nvidia/D216D77016D753D7/drone_ws/build/serial/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/media/nvidia/D216D77016D753D7/drone_ws/build/serial/test_results/serial/gtest-serial-test-timer.xml" "--return-code" "/media/nvidia/D216D77016D753D7/drone_ws/devel/.private/serial/lib/serial/serial-test-timer --gtest_output=xml:/media/nvidia/D216D77016D753D7/drone_ws/build/serial/test_results/serial/gtest-serial-test-timer.xml")
