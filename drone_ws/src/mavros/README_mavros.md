#MAVROS is the build environment for ROS with MavLink

Follow the tutorials on http://wiki.ros.org/ROS/Tutorials

1. Setup a ROS environment
2. Create a workspace
3. Run ROS nodes and topics
4. Publish abd subscribe messages
5. Create ROS nodes for sensors
