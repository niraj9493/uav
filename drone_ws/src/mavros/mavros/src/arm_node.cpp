#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/IsTargetAltReach.h>
#include "std_msgs/Float64.h"

//GLOBALS
mavros_msgs::State current_state;
mavros_msgs::CommandBool arm_cmd;
mavros_msgs::CommandTOL takeoff_cmd;
std_msgs::Float64 height_show;
mavros_msgs::IsTargetAltReach IsAltReached;
ros::Rate *rate;
bool fTookoff;

#define TARGET_HEIGHT_IN_METERS 4

void state_cb(const mavros_msgs::State::ConstPtr& msg)
{
    current_state = *msg;
}

void rel_alt(const std_msgs::Float64::ConstPtr& msg)
{
    height_show = *msg;
    // ROS_INFO("GOT %f \n",height_show.data;
}

ros::Subscriber alt;
ros::Subscriber state_sub;
ros::Publisher local_pos_pub;
ros::ServiceClient arming_client;
ros::ServiceClient set_mode_client;
ros::ServiceClient takeoff_client;

//my msg
ros::Publisher target_alt_reached_pub;


bool isArmed(void)
{
    return current_state.armed;
}

void arm(bool state)
{
        arm_cmd.request.value = state;
        if( arming_client.call(arm_cmd) &&  arm_cmd.response.success)
        {
            if(state) ROS_INFO("Vehicle ARMED");
            else      ROS_INFO("Vehicle DISArmed");
        }    
}

bool takeoff(float height)
{   
    if(height_show.data > 0.1 || fTookoff)
        return false;
    
    takeoff_cmd.request.min_pitch   = 0;
    takeoff_cmd.request.yaw         = 0;
    takeoff_cmd.request.latitude    = 0;
    takeoff_cmd.request.longitude   = 0;
    takeoff_cmd.request.altitude    = height;

    takeoff_client.call(takeoff_cmd);
 

    while(ros::ok() && height_show.data <= 0.98 * height)
	{
		    ROS_INFO("TAKEOFF -> ALT: %f",height_show.data);
	        ros::spinOnce();
        	rate->sleep();
	}
	
    IsAltReached.TargetAltAcheived = true;
    target_alt_reached_pub.publish(IsAltReached);

   if(takeoff_cmd.response.success)
        fTookoff = true;
    return takeoff_cmd.response.success;

}

bool land()
{   
    if(height_show.data < 0.1 || !fTookoff)
     {
         return false;
     }   
    
    takeoff_cmd.request.min_pitch   = 0;
    takeoff_cmd.request.yaw         = 0;
    takeoff_cmd.request.latitude    = 0;
    takeoff_cmd.request.longitude   = 0;
    takeoff_cmd.request.altitude    = 0.1;

    if( takeoff_client.call(takeoff_cmd) && takeoff_cmd.response.success)
	{
		ROS_INFO("LAND cmd sent ! ");
	}
    while(ros::ok() && height_show.data >= 0.95 * 0.1)
	{
		ROS_INFO("LAND -> ALT: %f",height_show.data);
	        ros::spinOnce();
        	rate->sleep();
	}

    arm(false);
    return takeoff_cmd.response.success;

}

void send_cmd(void)
{
    //TODO : take func pointer and exec every 5 seconds.
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "arm_node");
    ros::NodeHandle nh;

    fTookoff = false;
    state_sub = nh.subscribe<mavros_msgs::State>
            ("mavros/state", 10, state_cb);
    local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
            ("mavros/setpoint_position/local", 10);
    arming_client = nh.serviceClient<mavros_msgs::CommandBool>
            ("mavros/cmd/arming");
    set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
            ("mavros/set_mode");
    takeoff_client = nh.serviceClient<mavros_msgs::CommandTOL>
            ("mavros/cmd/takeoff");
    alt = nh.subscribe<std_msgs::Float64>
            ("mavros/global_position/rel_alt",10,rel_alt);
    //TARGET_ALT_MSG
    target_alt_reached_pub = nh.advertise<mavros_msgs::IsTargetAltReach>
            ("myTopicNiraj", 10);


    //the setpoint publishing rate MUST be faster than 2Hz
    rate = new ros::Rate(20.0);    

    ROS_INFO("Waiting to connect...");
    // wait for FCU connection
    while(ros::ok() && !current_state.connected){
        ros::spinOnce();
        rate->sleep();
    }

    ROS_INFO("CONNECTED ! ");

    geometry_msgs::PoseStamped pose;
    pose.pose.position.x = 0;
    pose.pose.position.y = 0;
    pose.pose.position.z = TARGET_HEIGHT_IN_METERS;

    //send a few setpoints before starting
    for(int i = 100; ros::ok() && i > 0; --i){
        local_pos_pub.publish(pose);
        ros::spinOnce();
        rate->sleep();
    }

    mavros_msgs::SetMode offb_set_mode;

    arm_cmd.request.value = true;

    ros::Time last_request = ros::Time::now();
	ROS_INFO("INIT");

    while(ros::ok())
    {
        if( current_state.mode != "GUIDED" && (ros::Time::now() - last_request > ros::Duration(5.0)))
        {       
            offb_set_mode.request.custom_mode = "GUIDED";
            if( set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent)
            {
                ROS_INFO("Offboard enabled");
            }
            last_request = ros::Time::now();
        } 
        else if( !current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0)))
        {
                if( arming_client.call(arm_cmd) && arm_cmd.response.success)
                {
                    ROS_INFO("Vehicle armed");
                }
                last_request = ros::Time::now();
            
        }
        else if( isArmed() && (ros::Time::now() - last_request > ros::Duration(5.0)))
        {

           takeoff(TARGET_HEIGHT_IN_METERS);
            
           ROS_INFO("Target Alt Reached- %f",height_show.data);
            
           ROS_INFO("attempting to land");

		
           last_request = ros::Time::now();
		if(fTookoff)
			break;
	


        }

	local_pos_pub.publish(pose);

        ros::spinOnce();
        rate->sleep();
    }

	//LAND
	while(ros::ok())
	{
		// if( current_state.mode != "LAND" && (ros::Time::now() - last_request > ros::Duration(5.0)))
       	// 	 {       
        //     		offb_set_mode.request.custom_mode = "LAND";
        //     		if( set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent)
        //     		{
        //         		ROS_INFO("LAND  enabled");
        //     		}
        //     		last_request = ros::Time::now();
        // 	} 
	

        // // ROS_INFO("Current Alt - %02.f",&height_show);

	       
        // local_pos_pub.publish(pose);

        ros::spinOnce();
        rate->sleep();
	
	}	
    

    return 0;
}
